﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour {
	public static Player instance;

	private Rigidbody rb;

	private float speed = 5.0f;
	private Vector2 input;

	private void Awake() {
		instance = this;

		rb = GetComponent<Rigidbody> ();
		rb.constraints = RigidbodyConstraints.FreezeRotation;
	}

	private void Update() {
		input = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical")) * speed;
	}

	private void FixedUpdate() {
		rb.velocity = new Vector3 (input.x, rb.velocity.y, input.y);
	}
}
