﻿using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
	private NavMeshAgent agent;

	void Awake() {
		agent = GetComponent<NavMeshAgent> ();
	}

	void Update() {
		agent.destination = Player.instance.transform.position;
	}
}
